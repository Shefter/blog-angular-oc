import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list-item-component',
  templateUrl: './post-list-item-component.component.html',
  styleUrls: ['./post-list-item-component.component.scss']
})
export class PostListItemComponentComponent implements OnInit {

  @Input() postName: string;
  @Input() postContent: string;
  @Input() postLikes: number;
  @Input() postDate: Date;

  constructor() { 
  }

  ngOnInit() {
  }

  incrementLike(){
    this.postLikes++;
  }

  decrementLike(){
    this.postLikes--;
  }

}
