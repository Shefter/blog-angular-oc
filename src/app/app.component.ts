import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  initialPosts = [
    {
      title: 'Messi loupe son péno !',
      content: 'Messi qui fait parti de l\'équipe d\'argentine a littéralement loupé son péno en coupe du monde.',
      loveIts: 0,
      created_at: new Date()
    },
    {
      title: 'La France qualifiée en 8ème de finale ! ',
      content: 'La France jouera donc samedi soir à 16h (heure française) contre l\'Argentine pour son match des 8èmes de finale.',
      loveIts: 0,
      created_at: new Date()
    },
    {
      title: 'Premier but de Mbappe !',
      content: 'Son rêve d\'enfant s\'est réalisé, Mbappe marque son tout premier but en coupe du monde !',
      loveIts: 0,
      created_at: new Date()
    }
  ];

}